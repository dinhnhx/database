'use strict'
const { json } = require('express');
const sql = require('mssql');
var config =
{
    server: 'fdsserver.database.windows.net',
    user: 'fdsadmin',
    password: 'FDS@2020',
    database: 'dbVasco',
    port: 1433,

};
const pool = new sql.ConnectionPool(config);
// var requery = new sql.Request(pool);
pool.connect().catch(function (err) {
    pool.close();
});



// connection.connect();
module.exports = {
    get: async (req, res) => {
        // console.log(req.params);
        // console.log(req.query);
        // let table = req.query.table;
        // const query = `SELECT * FROM [dbo].[Customers]`;

        let query = "EXEC [dbo].[VA_Getallinvoice] @order = NULL, @pageIndex = 0, @pageSize = 10,@total = 10";

        try {
            const recordset = await pool.query(query);
            console.log(recordset)
            res.json(recordset.recordsets);
        } catch (error) {
            console.log(error)
            res.json('no data')
        }

    },
    get_cash_flow: async (req, res) => {
        let query = "EXEC [dbo].[VA_CashFlow] @order = NULL, @pageIndex = 0, @pageSize = 10,@total = 10";

        try {
            const recordset = await pool.query(query)
            res.json(recordset.recordset);
        } catch (error) {
            res.json('no data')
        }

    },
    get_customers: async (req, res) => {
        const query = `SELECT * FROM [dbo].[Customers]`;

        try {
            const recordset = await pool.query(query)
            res.json(recordset.recordset);
        } catch (error) {
            console.log(error)
            res.json('no data')
        }

    },
    get_invoice: async (req, res) => {
        // console.log(req.params);
        // console.log(req.query);
        // let table = req.query.table;
        let query = "EXEC [dbo].[VA_Getallinvoice] @order = NULL, @pageIndex = 0, @pageSize = 10,@total = 10";

        try {
            const recordset = await pool.query(query);
            console.log(recordset)
            res.json(recordset.recordset);
            // res.
        } catch (error) {
            res.json('no data')
        }

    },
    get_1: async (req, res) => {
        let name = req.query.name;
        let query;
        console.log(name);
        if (name) {
            query = `SELECT 
            class.ClassName, students.StudentName 
            from tabClass as class RIGHT OUTER JOIN 
            tabStudent as students 
            ON students.ClassGuid = class.ClassGuid
            where students.StudentName ='${name}'`;

        }
        else {
            query = `SELECT 
            class.ClassName, students.StudentName 
            from tabClass as class RIGHT OUTER JOIN 
            tabStudent as students 
            ON students.ClassGuid = class.ClassGuid`;
        }
        try {
            const recordset = await pool.query(query)
            res.json(recordset.recordset)
        } catch (error) {
            res.json('no data')
        }
    },
    detail: async (req, res) => {

    },
    update_table: async (req, res) => {
        console.log('update_table')
        console.log(req.query);
        let name = req.query.name;
        let id = req.query.id;
        let email = req.query.email;
        let query = `UPDATE Customers SET Name = '${name}', Email='${email}' WHERE CustomerID= '${id}' `;
        console.log(query)
        try {
            const recordset = await pool.query(query).catch(function (err) {
                pool.close();
            });
            console.log(recordset)
            res.json('update success');
        } catch (error) {
            res.json('no data')
        }
    },

}
