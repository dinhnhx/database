'use strict';
module.exports = function (app) {
  let dbController = require('./controllers/ProductsController.js');

  // todoList Routes
  app.route('/gettable')
    .get(dbController.get_1);

  app.route('/update_table')
    .get(dbController.get);

  app.route('/api')
    .get(dbController.update_table);

  app.route('/invoice')
    .get(dbController.get_invoice);

  app.route('/customers')
    .get(dbController.get_customers);

  app.route('/cash_flow')
    .get(dbController.get_cash_flow);

    app.route('/test')
    .get(dbController.get);
};